**Desafio Web Selenium com Cucumber**

Testes automatizados dos serviços disponibilizados pela url: http://sampleapp.tricentis.com/101/app.php<br>

**Objetivo:**

Incluir uma cotação de seguro no sistema.<br>

**Recursos utilizados:**

 - Selenium-java; <br>
 - JUnit; <br>
 - Maven; <br>
 - Cucumber; <br>
 - Java 8; <br>
 - Chrome 90; <br>
 - Eclipse 2020-12; <br>
 - Cucumber Eclipe Plugin 1.0.0.202005150629. (Utilizado para Eclipse)<br>

**Como configurar o ambiente:**

 - Faça clone do projeto: https://gitlab.com/giduol/testewebcucumberselenium.git; <br>
 -Importe o projeto para sua IDE de preferência; <br>
 - Caso não utilize o Chrome versão 90, alterar o arquivo chromedriver na pasta src/Driver para a versão do Chrome que for utilizar; <br>
 - Necessário ter instalado o JDK8; <br>
 - Necessário instalar o Plugin do Cucumber.<br>

**Como configurar a execução da aplicação:**

 - Clicar com o botão direito na pasta src/test/resources/feature, "Run As", "Run Configurations";<br>

  <img src="RunAs.jpg"> <br>

 - Em "Project" selecionar o projeto TesteWebSelenium;<br>
 
 - Em "Feature Path" descrever o local da pasta de execução.<br>

 <img src="Path.jpg"> <br>

 **Como executar a aplicação:**

 - Executar os cenários na pasta src/test/resources/feature; <br>
   - Ou através do botão Run (Crtl+F11)
   

**Descrição do Projeto:**

 - Pasta src/test/java/given: Contém a execução do teste " Given que fiz acesso para contratacao"; <br>
 - Pasta src/test/java/tests: Contém todos steps utilizados <br>
                                                                
- Pasta src/Driver: Contém o chromedriver versão 90 do Chrome. <br>

** Evidências dos testes executados com sucesso:** <br>
 - Um teste com cadastro com sucesso (Positivo) <br>
 - Um teste com cadastro com erro (Negativo) <br> <br>
 
 <img src="testes_passados_com_sucesso.jpg"> <br>


