package pageObjects;

import elementMapper.EnterVehicleDataElementMapper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Web;
import static utils.Web.navegador;

public class EnterVehicleDataPage extends EnterVehicleDataElementMapper {

    public EnterVehicleDataPage() {PageFactory.initElements(Web.getCurrentDriver(), this);}

    public String validarcontadorveiculopage() {
        WebDriverWait wait = new WebDriverWait(navegador, 5);
        WebElement contadorpage = wait.until(ExpectedConditions.visibilityOf(contador));
        return contadorpage.getText();}

    public void incluirmarca(String marca) {make.sendKeys(marca);}

    public void incluirmodelo(String modelo) {model.sendKeys(modelo);}

    public void incluircilindrada(String cilindrada) {cylindercapacity.sendKeys(cilindrada);}

    public void incluirperformance(String perform) {engineperformance.sendKeys(perform);}

    public void incluirdatafabricacao(String datafabr) {dateofmanufacture.sendKeys(datafabr);}

    public void incluirnassentos(String assento) {numberofseats.sendKeys(assento);}

    public void incluiropcaodireita() {righthanddriveyes.click();}

    public void incluirnassentosmotoc(String nassentos) {numberofseatsmotorcycle.sendKeys(nassentos);}

    public void incluircombustivel(String combust) {fuel.sendKeys(combust);}

    public void incluircarga(String carga) {payload.sendKeys(carga);}

    public void incluirpesototal(String total) {totalweight.sendKeys(total);}

    public void incluirprecotabela(String preco) {listprice.sendKeys(preco);}

    public void incluirplaca(String placa) {licenseplatenumber.sendKeys(placa);}

    public void incluirkmanual(String km) {annualmileage.sendKeys(km);}

    public void clicarbtnnext() {btnnextenterinsurantdata.click();}
}
