package pageObjects;

import elementMapper.EnterInsurantDataElementMapper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Web;

import static utils.Web.navegador;

public class EnterInsurantDataPage extends EnterInsurantDataElementMapper {

    public EnterInsurantDataPage() {
        PageFactory.initElements(Web.getCurrentDriver(), this);}

    public String validarcontadorinsurantpage() {
        WebDriverWait wait = new WebDriverWait(navegador, 5);
        WebElement contadorpage = wait.until(ExpectedConditions.visibilityOf(contador));
        return contadorpage.getText();}

    public void incluirnome(String nome) {firstname.sendKeys(nome);}
    public void incluirsobrenome(String sobrenome) {lastname.sendKeys(sobrenome);}
    public void incluirdatanasc(String datanasc) {birthdate.sendKeys(datanasc);}
    public void incluirgenero() {gender.click();}
    public void incluirpais(String pais) {country.sendKeys(pais);}
    public void incluircep(String cep) {zipcode.sendKeys(cep);}
    public void incluirocupacao(String ocup) {occupation.sendKeys(ocup);}
    public void incluirhobby() {hobby.click();}
    public void clicarbtnnext() {btnnext.click();}
}
