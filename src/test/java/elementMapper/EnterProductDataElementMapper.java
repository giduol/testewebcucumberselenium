package elementMapper;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EnterProductDataElementMapper {

    @FindBy(css = "#enterproductdata > span")
    public WebElement contador;

    @FindBy(id = "startdate")
    public WebElement startdate;

    @FindBy(id = "insurancesum")
    public WebElement insurancesum;

    @FindBy(id = "meritrating")
    public WebElement meritrating;

    @FindBy(id = "damageinsurance")
    public WebElement damageinsurance;

    @FindBy(css = "#insurance-form > div > section:nth-child(3) > div.field.idealforms-field.idealforms-field-checkbox > p > label:nth-child(1)")
    public WebElement optional;

    @FindBy(id = "courtesycar")
    public WebElement courtesycar;

    @FindBy(id = "nextselectpriceoption")
    public WebElement btnnextprice;
}
