package elementMapper;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SelectPriceOptionElementMapper {

    @FindBy(css = "#selectpriceoption > span")
    public WebElement contador;

    @FindBy(css = "#priceTable > tfoot > tr > th.group > label:nth-child(4) > span")
    public WebElement ultimate;

    @FindBy(id = "nextsendquote")
    public WebElement btnnextquote;

}
