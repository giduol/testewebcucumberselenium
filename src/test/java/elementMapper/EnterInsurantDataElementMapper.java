package elementMapper;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EnterInsurantDataElementMapper {

    @FindBy(css = "#enterinsurantdata > span")
    public WebElement contador;

    @FindBy(id = "firstname")
    public WebElement firstname;

    @FindBy(id = "lastname")
    public WebElement lastname;

    @FindBy(id = "birthdate")
    public WebElement birthdate;

    @FindBy(css = "#insurance-form > div > section:nth-child(2) > div:nth-child(4) > p > label:nth-child(1) > span")
    public WebElement gender;

    @FindBy(id = "country")
    public WebElement country;

    @FindBy(id = "zipcode")
    public WebElement zipcode;

    @FindBy(id = "occupation")
    public WebElement occupation;

    @FindBy(css = "#insurance-form > div > section:nth-child(2) > div.field.idealforms-field.idealforms-field-checkbox > p > label:nth-child(1) > span")
    public WebElement hobby;

    @FindBy(id = "nextenterproductdata")
    public WebElement btnnext;
}
