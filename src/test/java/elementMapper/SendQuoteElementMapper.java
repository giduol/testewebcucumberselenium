package elementMapper;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SendQuoteElementMapper {

    @FindBy(id = "email")
    public WebElement email;

    @FindBy(id = "username")
    public WebElement username;

    @FindBy(id = "password")
    public WebElement password;

    @FindBy(id = "confirmpassword")
    public WebElement confirmpassword;

    @FindBy(id = "sendemail")
    public WebElement btnsendemail;

    @FindBy(css = "body > div.sweet-alert.showSweetAlert.visible > h2")
    public WebElement msgsucess;

    @FindBy(className = "confirm")
    public WebElement btnconfirm;

}
