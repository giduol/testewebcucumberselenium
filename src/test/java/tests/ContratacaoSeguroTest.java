package tests;

import given.ContratacaoSeguro;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.EnterVehicleDataPage;
import pageObjects.SelectPriceOptionPage;
import utils.Utils;
import utils.Web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import baseTests.BaseTests;

import static org.junit.Assert.assertEquals;
import static org.openqa.selenium.By.cssSelector;

public class ContratacaoSeguroTest extends BaseTests{


	@When("inserir dados aba Enter Vehicle Data")
	public void inserir_dados_Vehicle() throws Throwable {

	    add.incluirmarca("Honda");
		add.incluirmodelo("Motorcycle");
		add.incluircilindrada("500");
		add.incluirperformance("500");
		add.incluirdatafabricacao("01/30/2021");
		add.incluirnassentos("2");
		add.incluiropcaodireita();
		add.incluirnassentosmotoc("2");
		add.incluircombustivel("Other");
		add.incluircarga("20");
		add.incluirpesototal("200");
		add.incluirprecotabela("9000");
		add.incluirplaca("AAA0000");
		add.incluirkmanual("100000");
		assertEquals("0", add.validarcontadorveiculopage());
		add.clicarbtnnext();
	}

	@And("inserir dados aba Enter Insurant Data")
	public void inserir_dados_Insurante() throws Throwable {

		adddp.incluirnome("Giovanni");
        adddp.incluirsobrenome("Oliveira");
        adddp.incluirdatanasc("07/16/1983");
        adddp.incluirgenero();
        adddp.incluirpais("Brazil");
        adddp.incluircep("91110000");
        adddp.incluirocupacao("Employee");
        adddp.incluirhobby();
        assertEquals("0", adddp.validarcontadorinsurantpage());
        adddp.clicarbtnnext();
	}

	@And("inserir dados aba Enter Product Data")
	public void inserir_dados_Product() throws Throwable {

		addds.incluirdatainicio("06/30/2021");
        addds.selecionarvalorseguro("3.000.000,00");
        addds.selecionarclassificacao("Bonus 1");
        addds.selecionaropcapdanos("Full Coverage");
        addds.incluirprodutoopcional();
        addds.escolhercarrocortesia("Yes");
        assertEquals("0", addds.validarcontadorproductpage());
        addds.clicarbtnnextprice();
	}

	@And("inserir dados aba Select Price Option")
	public void inserir_dados_Price() throws Throwable {

        addp.escolherplano();
        assertEquals("0", addp.validarcontadorpricepage());
        addp.clicarbtnnextquote();
	}

	@And("inserir dados aba Send Quote")
	public void inserir_dados_Send_Quote() throws Throwable {
		addqp.inseriremail("giovanni@gmail.com");
        addqp.inserirusername("giovanni");
        addqp.inserirsenha("Teste123");
        addqp.confimarsenha("Teste123");
        addqp.clicarbtnenvio();
	}
	
	@And("inserir dados aba Send Quote com erro")
	public void inserir_dados_Send_Quote_com_erro() throws Throwable {
		addqp.inseriremail("giovanni@com");
        addqp.inserirusername("giovanni");
        addqp.inserirsenha("Teste123");
        addqp.confimarsenha("Teste123");
        addqp.clicarbtnenvio();
	}

	@Then("valido a mensagem de confirmacao e finalizo o envio")
	public void valido_mensagem_confirmacao_finalizo_envio() throws Throwable {
		assertEquals("Sending e-mail success!", addqp.pegarmsgconfirmacao());
		tearDown();
	}
	
	@Then("valido a mensagem de erro")
	public void valido_mensagem_erro() throws Throwable {
		assertEquals("Not finished yet...", addqp.pegarmsgconfirmacao());
		tearDown();
	}
}
